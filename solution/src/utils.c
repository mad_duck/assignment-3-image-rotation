#include "../include/utils.h"
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

void print_error(const char* string) {
	fprintf(stderr, "%s", string);
	fprintf(stderr, "\n");
}

void println(const char* string) {
	printf("%s", string);
	printf("\n");
}

void print_array(const char** array) {

	if (array == NULL) return;

	uint8_t size = 7 /*sizeof(**array) / sizeof(*array[0])*/, i;

	for (i = 0; i < size; i++){
		if (i == size - 1)
			printf("%s\n ", array[i]);
		printf("%s", array[i]);
	}

}

bool array_contains_string(const char* string, const char** array) {
	
	if (array == NULL) return false;
	
	uint8_t size = 7 /*sizeof(**array) / sizeof(*array[0])*/, i;

	for (i = 0; i < size; i++) {
		if (strcmp(string, array[i]) == 0)
			return true;
	}

	return false;
}

