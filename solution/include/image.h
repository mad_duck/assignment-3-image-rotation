#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>

// Pixel represention structure 
#pragma pack(push, 1)
struct pixel { 
	uint8_t b, g, r;
};
#pragma pack(pop)

// Image represention structure
struct image {
	uint64_t width, height;
	struct pixel* data;
};

// Image structure initialization function.
struct image init_image(const uint32_t width, const uint32_t height);

// Image copy function.
struct image copy_image(struct image const* img);

// Image destructor function(free memory).
void delete_image(struct image* img);

#endif /* IMAGE_H */

