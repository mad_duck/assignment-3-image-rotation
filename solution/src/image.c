#include "../include/image.h"
#include "../include/utils.h"
#include <stdlib.h>

// Image structure initialization function.
struct image init_image(const uint32_t width, const uint32_t height) {

    struct pixel* p = (struct pixel*)malloc(sizeof(struct pixel) * width * height);
    if (!p) {
        print_error("Cannot allocate the memory");
        exit(EXIT_SUCCESS);
    }

    return (struct image)
    {
        width,
        height,
        p
    };
}

// Image copy function.
struct image copy_image(struct image const* img) {

    struct image copy = init_image(img->width, img->height);

    if (!copy.data) {
        copy.width = 0;
        copy.height = 0;
        return copy;
    }

    uint64_t img_size = copy.width * copy.height, i;

    for (i = 0; i < img_size; i++)
        copy.data[i] = img->data[i];

    return copy;
}

// Image destructor function(free memory).
void delete_image(struct image* img) {
    if (img->data) {
        free(img->data);
        img->data = NULL;
    }
}

