#include "../include/bmp_handle.h"
#include "../include/utils.h"

#define BMP_SIGNATURE 0x4d42

// BMP image header representation structure
#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

// Calculate image padding bytes for bmp file function.
uint8_t get_padding(const uint64_t width) {
    const uint8_t bytes = 4;
    return (bytes - (width * sizeof(struct pixel)) % bytes) % bytes;
} 

// Reading image data from bmp file function.
enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    // Reading file header
    if (!fread(&header, sizeof(struct bmp_header), 1, in))
        return READ_HEADER_EXCEPTION;

    // Checking file signature
    if (header.bfType != BMP_SIGNATURE)
        return READ_SIGNATURE_EXCEPTION;

    // Creating copy of image 
    *img = init_image(header.biWidth, header.biHeight);

    // Calculating file padding
    uint8_t row_padding = get_padding(img->width);

    // Checking file data
    if (!img->data)
        return READ_MEMORY_ALLOCATION_EXCEPTION;

    // Reading pixels data 
    uint64_t i;
    for (i = 0; i < img->height; i++) {
        if (!fread(&img->data[(img->height - i - 1) * img->width], sizeof(struct pixel), img->width, in)) {
            delete_image(img);
            return READ_BITS_EXCEPTION;
        }

        fseek(in, row_padding, SEEK_CUR);
    }
    
    return READ_SUCCESS;
}

// Writing image data to bmp file function.
enum write_status to_bmp(FILE* out, struct image const* img) {

    // Calculating file padding 
    uint8_t row_padding = get_padding(img->width);

    // Setting bmp file header
    struct bmp_header header;
    header.bfType = BMP_SIGNATURE;
    header.biSizeImage = (uint32_t)(img->height * (img->width * sizeof(struct pixel) + row_padding));
    header.bfileSize = (uint32_t)(sizeof(struct bmp_header) + header.biSizeImage);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = sizeof(struct bmp_header) - 14;
    header.biWidth = (uint32_t)img->width;
    header.biHeight = (uint32_t)img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biXPelsPerMeter = 2835;
    header.biYPelsPerMeter = 2835;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
        return WRITE_HEADER_EXCEPTION;

    // Writing pixels in file
    uint64_t i, j;
    for (i = img->height; i > 0; i--) {
        if (!fwrite(&img->data[(i - 1) * img->width], sizeof(struct pixel), img->width, out))
            return WRITE_BITS_EXCEPTION;

        for (j = 0; j < row_padding; j++)
            fputc(0, out);
        
    }

    return WRITE_SUCCESS;
}

// Printing read status to stdout/stderr function.
void print_read_status(const enum read_status status) {
    switch (status) {
        case READ_SUCCESS:
            println("Successful read data");
            break;
        case READ_SIGNATURE_EXCEPTION:
            print_error("BMP file has invalid signature");
            break;
        case READ_BITS_EXCEPTION:
            print_error("Cannot read pixels bits of BMP file");
            break;
        case READ_HEADER_EXCEPTION:
            print_error("Cannot read header of BMP file");
            break;
        case READ_MEMORY_ALLOCATION_EXCEPTION:
            print_error("Cannot allocate memory");
            break;
        default:
            break;
    }
}

// Printing write status to stdout/stderr function.
void print_write_status(const enum write_status status) {
    switch (status) {
        case WRITE_SUCCESS:
            println("Successful writing data");
            break;
        case WRITE_HEADER_EXCEPTION:
            print_error("Cannot write header of BMP file");
            break;
        case WRITE_BITS_EXCEPTION:
            print_error("Cannot write pixels bits of BMP file");
            break;
        default:
            break;
    }
}

