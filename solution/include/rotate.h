#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"
#include <inttypes.h>

// Image rotation on angle in parameter.
void rotate(struct image* img, int16_t angle);

#endif /* ROTATE_H */

