#include "../include/utils.h"
#include "../include/bmp_handle.h"
#include "../include/rotate.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    FILE *in, *out;
    struct image img;
    enum read_status rstatus;
    enum write_status wstatus;
    const char* angles[] = {
        "0", "90", "180", "270", "-90", "-180", "-270"
    };

    if (argc != 4) {
        print_error("There must be 3 arguments in command line");
        return 1;
    }

    if ((in = fopen(argv[1], "rb")) == NULL) {
        print_error("Cannot read the file");
        return 1;
    }

    if (!array_contains_string(argv[3], angles)) {
        print_error("Wrong angle, there is no such in option"); 
        return 1;
    }
    
    rstatus = from_bmp(in, &img);
    fclose(in);

    print_read_status(rstatus);
    if (rstatus != READ_SUCCESS)
        return 1;

    rotate(&img, (int16_t)atoi(argv[3]));

    if (!img.data) {
        print_error("Error of memory allocatino");
        return 1;
    }

    if ((out = fopen(argv[2], "wb")) == NULL) {
        print_error("Cannot open the file");
        delete_image(&img);
        return 1;
    }

    wstatus = to_bmp(out, &img);
    fclose(out);

    print_write_status(wstatus);
    delete_image(&img);

    return 0;
}

