#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>

// Print string in stderr
void print_error(const char* string);

// Print string in stdout
void println(const char* string);

// Print all elements of array
void print_array(const char** array);

/* The method checks whether the array contains a string
*  RETURNS:
*       true - contains
*		false - does not contains
*/
bool array_contains_string(const char* string, const char** array);

#endif /* UTILS_H */

