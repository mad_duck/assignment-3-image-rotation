#include "../include/rotate.h"


// Image rotation on 90 degree 
void rotate90(struct image const* origin, struct image* goal) {

	if (origin->data && goal->data) {
		uint64_t i, j;
		for (i = 0; i < origin->height; i++) {

			for (j = 0; j < origin->width; j++)
				goal->data[j * origin->height + (origin->height - i - 1)] = origin->data[i * origin->width + j];

		}
	}
}

// Image rotation on angle in parameter.
void rotate(struct image* img, int16_t angle) {
	struct image rotated;
	int16_t count = (int16_t)(((360 + angle) % 360) / 90);

	int16_t i;

	for (i = 0; i < count; i++) {
		rotated = init_image(img->height, img->width);
		rotate90(img, &rotated);
		delete_image(img);
		*img = rotated;
	}

}

