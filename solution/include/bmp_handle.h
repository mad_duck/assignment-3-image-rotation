#ifndef BMP_HANDLE_H
#define BMP_HANDLE_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

// Statuses of reading bmp file information
enum read_status {
	READ_SUCCESS = 0,                   // Successful reading of image
	READ_SIGNATURE_EXCEPTION,			// If bmp signature is not 0x4d42 
	READ_BITS_EXCEPTION,				// Unable to read pixel bits 
	READ_HEADER_EXCEPTION,				// Unable to read header 
	READ_MEMORY_ALLOCATION_EXCEPTION	// Error of memory allocation for image
};

// Statuses of writing bmp file information 
enum write_status {
	WRITE_SUCCESS = 0,					// Successful reading of image 
	WRITE_HEADER_EXCEPTION,				// Unable to write header 
	WRITE_BITS_EXCEPTION				// Unable to write pixel bits 
};

// Reading image data from bmp file function.
enum read_status from_bmp(FILE* in, struct image* img);

// Writing image data to bmp file function.
enum write_status to_bmp(FILE* out, struct image const* img);

// Printing read status to stdin/stderr function.
void print_read_status(const enum read_status st);

// Printing write status to stdin/stderr function.
void print_write_status(const enum write_status st);

#endif /* BMP_HANDLE_H */

